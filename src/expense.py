class ExpenseItem:
  def __init__(self, id:str,expense:float):
    self.__id = id
    self.__expense = expense
  
  @property
  def expense(self):
    return self.__expense
