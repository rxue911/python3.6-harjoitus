import unittest
from src.expense import ExpenseItem

class ExpenseItemTest(unittest.TestCase):
  
  def test_expense(self):
    expenseItem = ExpenseItem("test", 15.5)
    self.assertEqual(expenseItem.expense, 15.5)
  
  def test_expense_as_str(self):
    expenseItem = ExpenseItem("test", "xxx")
    self.assertEqual(type(expenseItem.expense), str)
